package AES;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextArea;

import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;

import org.languagetool.JLanguageTool;
import org.languagetool.language.BritishEnglish;
import org.languagetool.rules.Rule;
import org.languagetool.rules.RuleMatch;



public class GrammarChecker extends masmt2.agent.MaSMTAgent {

	private String error;
	private String suggestion;
	MaSMTMessage tempmes;
	public static int errorcnt;
	public static String errors;
	public static List<String> ge;

	public GrammarChecker(String name,int id,String gp) {
		super(gp,name,id);

	}
	public GrammarChecker() {
		// TODO Auto-generated constructor stub
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}

	public void spellcheck(){
		JLanguageTool langTool = new JLanguageTool(new BritishEnglish());
		for (Rule rule : langTool.getAllRules()) {
		  if (!rule.isDictionaryBasedSpellingRule()) {
		    langTool.disableRule(rule.getId());
		  }
		}
		try{
			List<RuleMatch> matches = langTool.check("A speling error");
			for (RuleMatch match : matches) {
				  System.out.println("Potential typo at characters " +
				      match.getFromPos() + "-" + match.getToPos() + ": " +
				      match.getMessage());
				  System.out.println("Suggested correction(s): " +
				      match.getSuggestedReplacements());
				}
		}catch(IOException e){
			e.printStackTrace();
		}


	}

	public void grammar(String text, GrammarChecker checker, JTextArea area, Gui gui){

		JLanguageTool langTool = new JLanguageTool(new BritishEnglish());
		// comment in to use statistical ngram data:
		//langTool.activateLanguageModelRules(new File("/data/google-ngram-data"));

		try{
			List<RuleMatch> matches = langTool.check(text);
			List<String> gg = new ArrayList<String>();
			int noOfErrors = 1;

			for (RuleMatch match : matches) {

				checker.setError("Grammar Error " + String.valueOf(noOfErrors) + "\n" + match.getMessage() +
					       " at : {" + text.substring(match.getFromPos(), match.getToPos()) + "}");

				checker.setSuggestion("Suggested correction(s): " +
					      match.getSuggestedReplacements());

				area.append("\n"+checker.getError()+"\n"+checker.getSuggestion()+"\n");
				errors ="\n"+checker.getError()+"\n"+checker.getSuggestion();
				gg.add(checker.getError()+"\n"+checker.getSuggestion()+"\n");
				gui.HighLightTextRed(match.getFromPos(), match.getToPos(), area);
			 // System.out.println();
				errorcnt++;
				noOfErrors++;

			}
			ge=gg;
		}catch(IOException e){
			e.printStackTrace();
		}

	}

	@Override
	public void active() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void live() {
		// TODO Auto-generated method stub
		tempmes = waitForMessage();
	       if(tempmes.message.equals("sendGrammarDetails")){
	    	  // MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
		       MaSMTAbstractAgent gui =new MaSMTAbstractAgent("masmt", "gui",1);
		        //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
		       MaSMTMessage m =new MaSMTMessage(agent,gui,agent, 
		       "GrammarAgent", Integer.toString(GrammarChecker.errorcnt),"text", "broadcast");
		        sendMessage(m);
	       }
		
	}

}
