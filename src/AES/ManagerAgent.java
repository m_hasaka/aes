package AES;

import masmt2.message.MaSMTMessage;

/**
 *
 * @author Hasaka   dfdfdf
 */
public class ManagerAgent extends masmt2.agent.MaSMTManager {
    
    MaSMTMessage tempmes;
    private static Gui gui;
    private static ManagerAgent instance = null;
    private static final String MASMT = "masmt";

    public static ManagerAgent getSharedInstance() {
        if(instance == null) {
            instance = new ManagerAgent();
        }
        return instance;
    }

    private ManagerAgent() {
        super();
    }

    
    public static void main(String[] args) {
    	 gui =new Gui("gui",0, MASMT);
         gui.gstart(gui);
        
    }
    
    public static void nullObj(){
        instance = null;
    }

    @Override
    public void active() {
        System.out.println("[Active] MANAGER  .... " + super.agent);
        setNumberofClients(9);
        agents[0] = gui;
        agents[1] = new SetWordLimit("wordCount",1, MASMT);
        agents[2] = new GrammarChecker("Grammar",2,MASMT);
        agents[3] = new MultipleWords("mword",3,MASMT);
        agents[4] = new ProblemChecking("problemCheck",4,MASMT);
        agents[5] = new SVOextracter("svo",5,MASMT);
        agents[6] = new SolutionChecking("solCheck",6,MASMT);
        agents[7] = new MethodologyChecking("methoCheck", 7, MASMT);
        agents[8] = new Result("Result", 8, MASMT);
        
        activeAllClients();
        activeMessageParsing();
       
        
        
    }

    @Override
    public void live() {
        // empty method
    }

    @Override
    public void end() {
        // empty method
    }

}