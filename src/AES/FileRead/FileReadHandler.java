package AES.FileRead;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

/**
 * User: hasakam
 * Date: 16-Jul-18
 */
public class FileReadHandler {

    private static FileReadHandler self;

    private FileReadHandler() {
    }

    public static FileReadHandler getSharedInstance() {
        if (self == null) {
            self = new FileReadHandler();
            return self;
        } else {
            return self;
        }
    }

    public List<Element> readXml(String path, String child) {

        Document doc;
        Element root;
        List<Element> elements;

        String xmlFileName = "C:\\Program Files (x86)\\Aes\\app\\";
        xmlFileName += path;
        File xmlFile = new File(xmlFileName);
        try {
            if (xmlFile.exists()) {
                try (FileInputStream fis = new FileInputStream(xmlFile)) {

                    SAXBuilder sb = new SAXBuilder();
                    doc = sb.build(fis);
                    root = doc.getRootElement();
                    elements = root.getChildren(child);
                }
            } else {
                File xmlFile1 = new File(path);
                try (FileInputStream fis = new FileInputStream(xmlFile1)) {

                    SAXBuilder sb = new SAXBuilder();
                    doc = sb.build(fis);
                    root = doc.getRootElement();
                    elements = root.getChildren(child);
                }
            }
        } catch (Exception e) {
            elements = null;
            System.out.println("ERR:...");
        }
        return elements;
    }
}
