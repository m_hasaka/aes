package AES;

import AES.FileRead.FileReadHandler;
import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;
import org.jdom2.Element;

import java.util.List;


public class SolutionChecking extends masmt2.agent.MaSMTAgent {
	private int solCnt=0;
	
	public SolutionChecking(String name,int id,String gp) {
		super(gp,name,id);
	}

	@Override
	public void active() {
		//Auto-generated method stub
		
	}

	@Override
	public void end() {
		//Auto-generated method stub
		
	}

	@Override
	public void live() {
		System.out.println("[Live] .... " + super.agent);
		List<String> morpoWords;
       	morp mo =new morp();
        List<Element> elements;
        elements = FileReadHandler.getSharedInstance().readXml("solution.xml", "agent");
        String name;

		MaSMTMessage tempmes = waitForMessage();
		if(tempmes.message.equals("sendSolutionRelateDetails")) {
	    	String[] words = StatDetail.getInstance().WordFrmSentense();

	   		for(String word : words) {
	           	String [] parts = word.split(" ");

                for (Element element : elements) {
                    name = element.getAttributeValue("name");

                    for(String str : parts) {
                        morpoWords = mo.morphology(str);

                        for(String obj : morpoWords) {
                            if(name.equals(obj)) {
                                System.out.println(str + " solution is in " + word + " sentense");
                                ++solCnt;
                            }
                        }
                    }
                }
	   		}
	   		System.out.println("[Live] .... " + super.agent);
	        MaSMTAbstractAgent gui =new MaSMTAbstractAgent("masmt", "gui",1);
	        MaSMTMessage m =new MaSMTMessage(agent,gui,agent, 
	        "countOfSolutionRelateWords", Integer.toString(solCnt),"text", "broadcast");
	        sendMessage(m);
		}
	}
}
