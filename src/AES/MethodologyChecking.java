package AES;

import AES.FileRead.FileReadHandler;
import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;
import org.jdom2.Element;

import java.util.List;


public class MethodologyChecking extends masmt2.agent.MaSMTAgent {
	private int methoCnt = 0;
	
	public MethodologyChecking(String name,int id,String gp) {
		super(gp,name,id);
	}
	
	@Override
	public void active() {
		// Auto-generated method stub
		
	}

	@Override
	public void end() {
		// Auto-generated method stub
		
	}

	@Override
	public void live() {
        MaSMTMessage tempMes = waitForMessage();
        List<Element> elements;
        elements = FileReadHandler.getSharedInstance().readXml("methodology.xml", "agent");
        String name;

        if(tempMes.message.equals("sendMethodologyRelateDetails")) {
            String[] words = StatDetail.getInstance().WordFrmSentense();
            List<String> morpoWords;
            morp mo =new morp();

            for(String word : words) {
                String [] parts  = word.split(" ");

                for (Element element : elements) {
                    name = element.getAttributeValue("name");

                    for(String str : parts) {
                        morpoWords = mo.morphology(str);

                        for(String obj: morpoWords) {
                            if (name.equals(obj)) {
                                System.out.println(str + " methodology is in " + word + " sentense");
                                ++methoCnt;
                            }
                        }
                    }

                }
            }
            System.out.println("[Live] .... " + super.agent);
            MaSMTAbstractAgent gui = new MaSMTAbstractAgent("masmt", "gui",1);
            MaSMTMessage m = new MaSMTMessage(agent,gui,agent,
                    "countOfMethodologyRelateWords", Integer.toString(methoCnt),"text", "broadcast");
            sendMessage(m);

        }
	}
}
