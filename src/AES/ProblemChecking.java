package AES;

import AES.FileRead.FileReadHandler;
import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;
import org.jdom2.Element;

import java.util.List;


public class ProblemChecking extends masmt2.agent.MaSMTAgent {
	private int probCnt = 0;
	
	public ProblemChecking(String name,int id,String gp) {
		super(gp,name,id);
	}

	@Override
	public void active() {
		//Auto-generated method stub
		
	}

	@Override
	public void end() {
		//Auto-generated method stub
		
	}

	@Override
	public void live() {
        System.out.println("[Live] .... " + super.agent);

        MaSMTMessage tempmes = waitForMessage();
        List<Element> elements;
        elements = FileReadHandler.getSharedInstance().readXml("problem.xml", "agent");
        String name;

        if (tempmes.message.equals("sendProblemRelateDetails")) {
            System.out.println("sendProblemRelateDetails----------------");
            morp mo = new morp();
            String [] words = StatDetail.getInstance().WordFrmSentense();
            List<String> morpoWords;
            try {
                for (String word : words) {
                    String[] parts = word.split(" ");

                    for (Element element : elements) {
                        name = element.getAttributeValue("name");

                        for (String str : parts) {
                            morpoWords = mo.morphology(str);

                            for (String obj : morpoWords) {
                                if (name.equals(obj)) {
                                    System.out.println(str + " is in " + word + " sentense");
                                    System.out.println("[Live] .... " + super.agent);
                                    MaSMTAbstractAgent svoAgnt = new MaSMTAbstractAgent("masmt", "svo", 1);
                                    MaSMTMessage m1 = new MaSMTMessage(agent, svoAgnt, agent,
                                            "ProblemRelateSentence", word, str, "broadcast");
                                    sendMessage(m1);
                                    ++probCnt;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("[Live] .... " + super.agent);
                MaSMTAbstractAgent gui = new MaSMTAbstractAgent("masmt", "gui", 1);
                MaSMTMessage m = new MaSMTMessage(agent, gui, agent,
                        "countOfProblemRelateWords", Integer.toString(probCnt), "NoSvo", "broadcast");
                sendMessage(m);
            }

            if (probCnt == 0) {
                System.out.println("[Live] .... " + super.agent);
                MaSMTAbstractAgent gui = new MaSMTAbstractAgent("masmt", "gui", 1);
                MaSMTMessage m = new MaSMTMessage(agent, gui, agent,
                        "countOfProblemRelateWords", Integer.toString(probCnt), "NoSvo", "broadcast");
                sendMessage(m);

            }

        } else if (tempmes.message.equals("ProblemSituation")) {
            String probSituation = tempmes.content;
            System.out.println("[Live] .... " + super.agent);
            MaSMTAbstractAgent gui = new MaSMTAbstractAgent("masmt", "gui", 1);
            MaSMTMessage m = new MaSMTMessage(agent, gui, agent,
                    "countOfProblemRelateWords", Integer.toString(probCnt), probSituation, "broadcast");
            sendMessage(m);
        }
    }
}
