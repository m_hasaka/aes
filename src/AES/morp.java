package AES;

import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.impl.file.Morphology;

import java.util.ArrayList;
import java.util.List;


public class morp {
	public static void main(String[] args) {
		morp m = new morp();
		for(String obj: m.morphology("cries")){
			System.out.println(obj);
		}
	}
	public List<String> morphology(String word) {
		System.setProperty("wordnet.database.dir", "dict");
		//WordNetDatabase database = WordNetDatabase.getFileInstance();
 
		Morphology id = Morphology.getInstance();
 
		String[] arr = id.getBaseFormCandidates(word, SynsetType.NOUN);
		List<String> morpo = new ArrayList<String>();
		//System.out.println(arr.length);
		if(arr.length!=0){
			for(String a: arr){
				//System.out.println(a);
				morpo.add(a);
			}
			return morpo;
		}else{
			morpo.add(word);
			return morpo;
		}
			
	}
}
