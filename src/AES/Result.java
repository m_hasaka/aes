package AES;

import AES.FileRead.FileReadHandler;
import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;
import org.jdom2.Element;

import java.util.List;


public class Result extends masmt2.agent.MaSMTAgent {
	private int ResCnt = 0;
	private MaSMTMessage tempmes;
	
	public Result(String name,int id,String gp) {
		super(gp,name,id);
	}

	@Override
	public void active() {
		//Auto-generated method stub
		
	}

	@Override
	public void end() {
		//Auto-generated method stub
		
	}

	@Override
	public void live() {
		tempmes = waitForMessage();
		List<Element> elements;
		elements = FileReadHandler.getSharedInstance().readXml("result.xml", "agent");
		String name;

		if(tempmes.message.equals("sendResultRelateDetails")) {
	    	List<String> morpoWords;
	        morp mo =new morp();
	    	String[] words1 = StatDetail.getInstance().WordFrmSentense();

	   		for(int i=0; i< StatDetail.getInstance().count1; i++) {
	           	String [] parts = words1[i].split(" ");

                for (Element element : elements) {
                    name = element.getAttributeValue("name");

                    for(String str : parts) {
                        morpoWords = mo.morphology(str);

                        for(String obj: morpoWords) {
                            if(name.equals(obj)) {
                                System.out.println(str + " result is in "+i+" sentense");
                                ++ResCnt;
                            }
                        }
                    }
                }
	   		}
	   		System.out.println("[Live] .... " + super.agent);
	        MaSMTAbstractAgent gui =new MaSMTAbstractAgent("masmt", "gui",1);
	        MaSMTMessage m =new MaSMTMessage(agent,gui,agent, 
	        "countOfResultRelateWords", Integer.toString(ResCnt),"text", "broadcast");
	        sendMessage(m);
		}
	}
}
