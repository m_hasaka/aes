package AES;

import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;


public class SetWordLimit extends masmt2.agent.MaSMTAgent implements ActionListener {

	private JTextField minval;
	private JTextField maxval;
	private JButton btnOk, btnCancel;
	MaSMTMessage tempmes;
    static int msg,rem1;
	private JDialog dialog;


	public SetWordLimit(JFrame frame) {
		createUI(frame);
	}

	public SetWordLimit() {
	}

	public void createUI(JFrame f) {
		dialog = new JDialog();
		dialog.setTitle("Set min max values");
		dialog.setPreferredSize(new Dimension(335, 175));
		dialog.setResizable(false);
		dialog.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 5, 5));
		dialog.pack();
		dialog.setLocationRelativeTo(f);
		dialog.add(createPanel());
		dialog.setVisible(true);
	}

	private JPanel createPanel() {
		JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"130", "165"}, new String[]{"25", "25", "25", "100%"}, 5, 5));
		JPanel valueAndBtnPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "25"}));
		minval = new JTextField();
		maxval = new JTextField();

		topPanel.add(new JLabel("Minimum Value"));
		topPanel.add(minval);

		topPanel.add(new JLabel("Maximum Value"));
		topPanel.add(maxval);

		topPanel.add(Box.createGlue());
		topPanel.add(createBtnPanel());

		topPanel.add(valueAndBtnPanel);

		return topPanel;
	}

	private JPanel createBtnPanel() {
		JPanel btnPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "80", "5", "80"}, new String[]{"25"}));
		btnPanel.add(Box.createHorizontalGlue());

		btnOk = new JButton("OK");
		btnOk.addActionListener(this);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(this);

		btnPanel.add(btnCancel);
		btnPanel.add(Box.createHorizontalGlue());
		btnPanel.add(btnOk);

		return btnPanel;
	}

	public SetWordLimit(String name,int id,String gp) {
		super(gp,name,id);
        
	}
	
	public void checkLimit(int newlines, int coun, JTextArea ar,Gui a, StatDetail st,int min,int max){
		if(min>coun){
			int rem =min - coun;
			rem1 = min-coun;
			ar.append("\n\nPlease add "+ rem + " or more words");
//			a.HighLightTextRed(13+newlines,15+newlines,st,ar);
			//msg = "pleaseAddWords";
			msg=st.count1;
		}else if(max<coun){
			int rem = coun - max;
			rem1=max-coun;
			ar.append("\n\nPlease remove "+rem+" or more words");
//			a.HighLightTextRed(16+newlines,18+newlines,st,ar);
			//msg = "pleaseRemoveWords";
			msg=st.count1;
		}else{
			rem1=0;
			ar.append("\n\nwords are within the limits ");
//			a.HighLightTextGreen(2+newlines,29+newlines,st,ar);
			//ab.setMsg("wordsAreInLimit");
			msg=st.count1;
			
			//System.out.println("msg!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! "+msg);
			
		}
	}
	
	private static Node getCompany(Document doc, String id, String name) {
        Element company = doc.createElement("Val");
        company.setAttribute("minVal", id);
        company.setAttribute("maxVal", name);
        return company;
    }


	@Override
	public void active() {
		 System.out.println("[Live] .... " + super.agent);
	}

	@Override
	public void end() {
		
	}

	@Override
	public void live() {
		
		tempmes = waitForMessage();
	       if(tempmes.message.equals("sendDetails")){
	    	  // MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
		       MaSMTAbstractAgent gui =new MaSMTAbstractAgent("masmt", "gui",1);
		        //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
		       MaSMTMessage m =new MaSMTMessage(agent,gui,agent, 
		       "NoOfSentences", Integer.toString(SetWordLimit.msg),Integer.toString(rem1), "broadcast");
		        sendMessage(m);
		        
		        /*MaSMTMessage m1 =new MaSMTMessage(agent,gui,agent, 
		 		"WordLimitRange", msg1,"text", "broadcast");
		 		sendMessage(m1);*/
	       }

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btnOk)) {
			btnOkActionPerformed();
			dialog.setVisible(false);
		} else if (e.getSource().equals(btnCancel)) {
			dialog.setVisible(false);
		}
	}

	private void btnOkActionPerformed() {
		DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder icBuilder;
		try {
			icBuilder = icFactory.newDocumentBuilder();
			Document doc = icBuilder.newDocument();
			// Element mainRootElement = doc.createElementNS("aa", "Values");
			Element mainRootElement = doc.createElement("Values");
			doc.appendChild(mainRootElement);

			// append child elements to root element
			mainRootElement.appendChild(getCompany(doc, minval.getText(), maxval.getText()));

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("C:\\Program Files (x86)\\Aes\\app\\Values.xml"));
			transformer.transform(source, result);

			System.out.println("\nXML DOM Created Successfully..");

		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(dialog, "Please insert Values.xml");
		}
		//System.out.println(minVal+" "+maxVal);
	}
}
